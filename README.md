Yet another fork of [egzumers](https://github.com/egzumer/uv-k5-firmware-custom) _excellent_ firmware for the Quansheng
UV K5, which itself is a merge of [OneOfEleven custom firmware](https://github.com/OneOfEleven/uv-k5-firmware-custom)
with [fagci spectrum analizer](https://github.com/fagci/uv-k5-firmware-fagci-mod/tree/refactor) with a few changes. This
fork specifically, combines work from other forks of egzumers firmware. So far, it contains features and fixes
from [armel](https://github.com/armel/uv-k5-firmware-custom)
and [AubsUK](https://github.com/AubsUK/uv-k5-firmware-custom).

All is a cloned and customized version of DualTachyon's open firmware
found [here](https://github.com/DualTachyon/uv-k5-firmware).

# Manual

Go to [AubsUK's](https://github.com/AubsUK/uv-k5-firmware-custom) repo to see how to use the 10 scan
lists, [Armel's](https://github.com/armel/uv-k5-firmware-custom) repo for all of their changes (there's a lot), and
to [Egzumer's](https://github.com/egzumer/uv-k5-firmware-custom) repo for everything else.

# Chirp driver

You can use [this](https://codeberg.org/arek/uv-k5-chirp-driver/raw/branch/main/uvk5_arek.py) to program frequencies in.
It's just [AubsUK's](https://github.com/AubsUK/uvk5-chirp-driver) driver, with one variable changed to make it work. You
won't be able to make changes to any of Armels features. You _might_ be able to
use [Armels](https://github.com/armel/uv-k5-chirp-driver) driver, and tick to only upload their changes, but I've not
tested it.

# License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.