/* Copyright 2023 Dual Tachyon
 * https://github.com/DualTachyon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

#include <string.h>

#include "app/chFrScanner.h"
#ifdef ENABLE_FMRADIO
#include "app/fm.h"
#endif
#include "app/scanner.h"
#include "bitmaps.h"
#include "driver/keyboard.h"
#include "driver/st7565.h"
#include "external/printf/printf.h"
#include "functions.h"
#include "helper/battery.h"
#include "misc.h"
#include "settings.h"
#include "ui/battery.h"
#include "ui/helper.h"
#include "ui/status.h"
#include "ui/ui.h"

static void convertTime(uint8_t *line, uint8_t type) {
  char str[8] = "";

  uint8_t m, s; // Declare variables for seconds, hours, minutes, and seconds
  uint16_t t;

  if (type == 0) // Tx timer
    t = (gTxTimerCountdown_500ms / 2);
  // t = ((gEeprom.TX_TIMEOUT_TIMER + 1) * 5) - (gTxTimerCountdown_500ms / 2);
  else // Rx timer
    t = 3600 - (gRxTimerCountdown_500ms / 2);

  m = t / 60;
  s = t - (m * 60);

  gStatusLine[14] = 0x00; // Quick fix on display (on scanning I, II, etc.)

  sprintf(str, "%02d:%02d", m, s);
  UI_PrintStringSmallBufferNormal(str, line + 0);

  gUpdateStatus = true;
}

void UI_DisplayStatus() {
  char str[8] = "";

  gUpdateStatus = false;
  memset(gStatusLine, 0, sizeof(gStatusLine));

  uint8_t *line = gStatusLine;
  unsigned int curPos, nextPos = 0;
  // **************

  if (gScanStateDir != SCAN_OFF && IS_MR_CHANNEL(gNextMrChannel) &&
      !SCANNER_IsScanning() && gCurrentFunction != FUNCTION_TRANSMIT &&
      !FUNCTION_IsRx()) {
    char s[11];
    sprintf(s, "");
    for (uint8_t i = 0; i < 10; i++) {
      if (currentScanList == i) {
        if (gEeprom.SCAN_LISTS[i]) {
          s[i] = i + '0';
        } // If the currentScanList is enabled, show the number
        else {
          s[i] = '-';
        } // Otherwise, show a hyphen (When ChanScanning it loops all lists
          // then stops, leaving currentScanList populated)
      } else {
        s[i] = gEeprom.SCAN_LISTS[i] ? '*' : '-';
      } // show an asterisk if it's enabled, or a hyphen if not
    }

    s[10] = '\0'; // terminate the string
    UI_PrintStringSmallBufferNormal(
        s, line); // Added -9 to move it to the far left
    nextPos = 10;
  } else {
    // POWER-SAVE indicator
    if (gCurrentFunction == FUNCTION_POWER_SAVE) {
      memcpy(line + curPos, gFontPowerSave, sizeof(gFontPowerSave));
    }
    curPos += 8;
    nextPos = curPos;

#ifdef ENABLE_NOAA
    if (gIsNoaaMode) { // NOASS SCAN indicator
      memcpy(line + curPos, BITMAP_NOAA, sizeof(BITMAP_NOAA));
      nextPos = curPos + sizeof(BITMAP_NOAA);
    }
    curPos += sizeof(BITMAP_NOAA);
#endif

#ifdef ENABLE_DTMF_CALLING
    if (gSetting_KILLED) {
      memset(line + curPos, 0xFF, 10);
      nextPos = curPos + 10;
    } else
#endif
#ifdef ENABLE_FMRADIO
        if (gFmRadioMode) { // FM indicator
      memcpy(line + curPos, gFontFM, sizeof(gFontFM));
      nextPos = curPos + sizeof(gFontFM);
    }
#endif
    curPos += 10; // font character width

#ifdef ENABLE_VOICE
                  // VOICE indicator
    if (gEeprom.VOICE_PROMPT != VOICE_PROMPT_OFF) {
      memcpy(line + curPos, BITMAP_VoicePrompt, sizeof(BITMAP_VoicePrompt));
      nextPos = curPos + sizeof(BITMAP_VoicePrompt);
    }
    curPos += sizeof(BITMAP_VoicePrompt);
#endif

    if (!SCANNER_IsScanning()) {
      if (gCurrentFunction == FUNCTION_TRANSMIT) {
        convertTime(line, 0);
      } else if (FUNCTION_IsRx()) {
        convertTime(line, 1);
      } else {
        uint8_t dw = (gEeprom.DUAL_WATCH != DUAL_WATCH_OFF) +
                     (gEeprom.CROSS_BAND_RX_TX != CROSS_BAND_OFF) * 2;
        if (dw == 1 || dw == 3) { // DWR - dual watch + respond
          if (gDualWatchActive)
            memcpy(line + curPos + (dw == 1 ? 0 : 2), gFontDWR,
                   sizeof(gFontDWR) - (dw == 1 ? 0 : 5));
          else
            memcpy(line + curPos + 3, gFontHold, sizeof(gFontHold));
        } else if (dw == 2) { // XB - crossband
          memcpy(line + curPos + 2, gFontXB, sizeof(gFontXB));
        } else {
          memcpy(line + curPos + 2, gFontMO, sizeof(gFontMO));
        }
      }
    }
    curPos += sizeof(gFontDWR) + 3;

#ifdef ENABLE_VOX
    // VOX indicator
    if (gEeprom.VOX_SWITCH) {
      memcpy(line + curPos, gFontVox, sizeof(gFontVox));
      nextPos = curPos + sizeof(gFontVox) + 1;
    }
    curPos += sizeof(gFontVox) + 3;
#endif

#ifdef ENABLE_FEAT_F4HWN
    // PTT indicator
    if (gSetting_set_ptt_session) {
      memcpy(line + curPos, gFontPttOnePush, sizeof(gFontPttOnePush));
      nextPos = curPos + sizeof(gFontPttOnePush) + 1;
    } else {
      memcpy(line + curPos, gFontPttClassic, sizeof(gFontPttClassic));
      nextPos = curPos + sizeof(gFontPttClassic) + 1;
    }
    curPos += sizeof(gFontPttClassic) + 3;
#endif
  }
  curPos = MAX(nextPos, 70u);

  // KEY-LOCK indicator
  if (gEeprom.KEY_LOCK) {
    memcpy(line + curPos + 1, gFontKeyLock, sizeof(gFontKeyLock));
  } else if (gWasFKeyPressed) {
    memcpy(line + curPos + 1, gFontF, sizeof(gFontF));
    /*
    UI_PrintStringSmallBufferNormal("F", line + x + 1);

    for (uint8_t i = 71; i < 79; i++)
    {
            gStatusLine[i] ^= 0x7F;
    }
    */
  } else if (gBackLight) {
    memcpy(line + curPos + 1, gFontLight, sizeof(gFontLight));
  } else if (gChargingWithTypeC) {
    memcpy(line + curPos + 1, BITMAP_USB_C, sizeof(BITMAP_USB_C));
  }

  // Battery
  unsigned int batPos = LCD_WIDTH - sizeof(BITMAP_BatteryLevel1) - 0;

  UI_DrawBattery(line + batPos, gBatteryDisplayLevel, gLowBatteryBlink);

  switch (gSetting_battery_text) {
  default:
  case 0:
    break;

  case 1: { // voltage
    const uint16_t voltage = (gBatteryVoltageAverage <= 999)
                                 ? gBatteryVoltageAverage
                                 : 999; // limit to 9.99V
#ifdef ENABLE_FEAT_F4HWN
    sprintf(str, "%u.%02u", voltage / 100, voltage % 100);
#else
    sprintf(str, "%u.%02uV", voltage / 100, voltage % 100);
#endif
    break;
  }

  case 2: // percentage
    sprintf(str, "%u%%", BATTERY_VoltsToPercent(gBatteryVoltageAverage));
    break;
  }

  batPos -= (7 * strlen(str));
  UI_PrintStringSmallBufferNormal(str, line + batPos);

  // **************

  ST7565_BlitStatusLine();
}
